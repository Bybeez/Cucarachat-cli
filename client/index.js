const io = require('socket.io-client');
const inquirer = require('inquirer');
const colors = require('colors');
const program = require('commander')
const fs = require('fs');
let pseudo;
let ip = "http://";
let socket = io(ip);
let disconnected = false;
let question

// Configuration des paramètres attendus
program
    .version('0.0.1')
    .option('-p, --pseudo [pseudo]', 'pseudo to use')


program.parse(process.argv)


if (!program.pseudo) {
    fs.readFile('pseudo.txt', 'utf8', (err, data) => {
        if (err) {
            console.log("Fichier pseudo non trouvé : demande")
            question = [
                {
                    type: 'input',
                    name: 'pseudo',
                    message: "Quel pseudo voulez vous utiliser ?"
                }
            ];

            inquirer.prompt(question).then(answers => {
                if (answers['pseudo'] != null) {
                    pseudo = answers['pseudo'];
                    fs.writeFile('pseudo.txt', 'utf8', (err, data) => {
                        if (err) {
                            console.log('erreur d ecriture '+err);
                            stop();
                        }

                        });
                    recupip();
                    return true;
                }
            });
        }

        else {
            pseudo = data;
            recupip();
            return true
        }
    });

}

else {
    pseudo = program.pseudo;
    recupip();
}


function recupip() {
    question = [
        {
            type: 'input',
            name: 'Connection',
            message: "Quelle est l'adresse ip du serveur ?"
        }
    ];

    inquirer.prompt(question).then(answers => {
        if (answers['Connection'] != null) {
            ip += answers['Connection'];
            socket = io(ip);
            connect()
        }
    });

}

function connect() {
    console.log('Connexion en cours');
    socket.on('connect', function () {
        if (disconnected) {
            console.log("Serveur reconnecté".green);
            disconnected = false;
            askformessage()
        }
        else {
            console.log("Connexion reussie".green);
            events()
        }

    })
}


function events() {
    socket.on('messages', function (messages) {
        messages.forEach(function (message) {
            console.log('\n');
            console.log('//////////////////////////');
            console.log(message.pseudo.green);
            console.log(message.message.white)
            console.log('//////////////////////////');
            console.log('\n');
        })
    })

    socket.on('disconnect', function () {
        disconnected = true;
        console.log('\n');
        console.log('//////////////////////////');
        console.log('Déconnecté du serveur'.red);
        console.log('Reconnexion en cours'.yellow);
        console.log('//////////////////////////');
        console.log('\n');
    })

    socket.on('message', function (message) {
        console.log('\n');
        console.log('//////////////////////////');
        console.log(message.pseudo.green);
        console.log(message.message.white)
        console.log('//////////////////////////');
        console.log('\n');
    })

    askformessage()
}


function sendmessage(message) {
    if (!disconnected) {
        socket.emit('userMessage', message);
        askformessage()
    }
}


function askformessage() {
    if (!disconnected) {
        let message = "";
        question = [
            {
                type: 'input',
                name: 'Message',
                message: "Quel est votre message ?"
            }
        ];
        inquirer.prompt(question).then(answers => {
            message = {
                pseudo: pseudo,
                message: answers.Message,
                channel: 1
            }
            sendmessage(message)
        });
    }
}

    

