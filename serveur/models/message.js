const Sequelize = require('sequelize')
const db = require('../db')
const Message = db.define('message', {
  pseudo: {
    type: Sequelize.STRING
  },
  message: {
    type: Sequelize.STRING
  },
  channel: {
    type: Sequelize.INTEGER
  }
})

module.exports = Message
