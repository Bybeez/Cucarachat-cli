#!/usr/bin/env node
const socket = require('socket.io');
const Message = require('./models/message')
const http = require('http');
const program = require('commander')

// Configuration des paramètres attendus
program
.version('0.0.1')
.option('-H, --history [count]', 'How many messages must be sent as history')
.option('-p, --port [port]', 'server listening port')
// On parse (convertit en format utilisable) les options
// fonction synchrone


program.parse(process.argv)
const historyCount = (program.count) ? program.count : 30;
const port = (program.port) ? program.port : 8080;

const app = http.createServer();
const io = socket(app);
app.listen(port);
console.log("Server started");

io.on('connection',function (socket) {
  console.log("Socket opened : "+socket.id);

  //Connect db, fetch 30 last messages, send them to client
  Message.findAll({
    order: ['createdAt'],
    limit: historyCount
  }).then(messages => {
    socket.emit('messages', messages)
  })


  socket.on('userMessage', function(message){
    if(message.message && message.pseudo && message.channel)
    {
      console.log("Message received");
      Message.create(message)
      io.emit('message', message)
    }
  })
})
