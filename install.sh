#!/usr/bin/env bash
echo "Demarrage de l'installation"
cd client
npm install
echo "Fin de l'installation du client"
echo "Lancement client"
x-terminal-emulator -e node index.js
cd ../serveur/
npm install
echo "Fin de l'installation"
echo "Lancement serveur"
x-terminal-emulator -e node index.js
